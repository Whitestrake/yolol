# Whitestrake's Starbase YOLOL scripts

The scripts in this repository are a collection of YOLOL scripts for use on ships in Starbase.

Individual scripts are named according to their purpose and minimum YOLOL chip level, and are usually commented to highlight their functionality, inputs and outputs, and tunable variables where applicable.

Tunables are usually set appropriately for my own usage; users beware.
